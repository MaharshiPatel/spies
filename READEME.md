# Project S.P.I.E.S.
System Performance and Information Electronic Surveillence

# Important Information
## IP Addresses

|Service    |External Hostname      |Internal IP   |Port                                                      |
|-----------|-----------------------|--------------|----------------------------------------------------------|
|Docker Reg |dockerreg.conygre.com  |172.31.36.223 |NA                                                        |
|ELK Stack  |elk.conygre.com        |172.31.32.76  |9200,9300,5601                                            |
|ITRS       |itrs.conygre.com       |172.31.0.28   |55801-55809(GW),55901-55909(NP),8050,8060,8070,8080 (WEB) |
|Dev OS     |dev1.conygre.com       |172.31.16.122 |8443                                                      |
|UAT OS     |uat.conygre.com        |172.31.28.227 |8443                                                      |  
|PROD OS    |appsrvprod.conygre.com |172.31.45.142 |8443                                                      |
|Yahoo Feed |feed.conygre.com       |172.31.46.35  |8080                                                      |
|uDeploy    |udeploy.conygre.com    |172.31.3.213  |8080                                                      |

# Phase 1
## Project Plan/Task Assignment
| Task     		 | Detail 																				| Person 	 	| Planned Start Date 	  	| Due Date 			   	| Status 	 |
| ---------------| -------------------------------------------------------------------------------------| --------------| ------------------------	| ----------------------| -----------|
| uDeploy  	  	 | Automate 1-click deployment to UAT 	  										    	|  Maharshi / Yi| Thursday, July 18 2019 	|Tuesday, July 23 2019 	| In Progress| 
| uDeploy 		 | Automate 1-click deployment to PROD  	  	  										|  Maharshi / Yi| Thursday, July 18 2019 	|Tuesday, July 23 2019 	| In Progress| |
| uDeploy 		 | Develop diagram of  pipeline to UAT/PROD   	  	 									|  Maharshi / Yi| Thursday, July 18 2019 	|Tuesday, July 23 2019 	| In Progress| |
| uDeploy		 | Setting up uDeploy pipeline Demonstrate environment specific configuration settings	|  Maharshi / Yi| Thursday, July 18 2019 	|Tuesday, July 23 2019 	| In Progress| |
| ITRS Monitoring| Establishing managed entities and samplers  	  	 									|  Jessica		| Thursday, July 18 2019 	|Tuesday, July 23 2019	| In Progress| |
| ITRS Monitoring| Creation of rules to reflect system state 	  	 									|  Jessica		| Thursday, July 18 2019 	|Tuesday, July 23 2019 	| In Progress| |
| ITRS Monitoring| Create dashboards 	  																|  Jessica		| Thursday, July 18 2019 	|Tuesday, July 23 2019 	| In Progress| |
| ELK Monitoring | Capture of trending data from environment server logs 	  	 						|  Fiona		| Thursday, July 18 2019 	|Tuesday, July 23 2019 	| In Progress| |
| ELK Monitoring | Create dashboards 	  	 															|  Fiona		| Thursday, July 18 2019 	|Tuesday, July 23 2019 	| In Progress| |
| Documentation  | Application and environment specific documentation 	  	 							|  Everyone		| Thursday, July 18 2019 	|Tuesday, July 23 2019 	| In Progress| |
| Documentation  | Capture documentation in GitBucket shared depository 	  							|  Everyone		| Thursday, July 18 2019 	|Tuesday, July 23 2019 	| In Progress| |
| Documentation  | Record Lessons learned 	  	 														|  Everyone		| Thursday, July 18 2019 	|Tuesday, July 23 2019 	| In Progress| |

## System Architechture
See diagram file in repository.