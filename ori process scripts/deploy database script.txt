#!/bin/bash

# Deploy ActiveMQ

if [[ ${p:DBDEPLOY} == 'y' ]]
then
    # Set the trades image stream
    cat >is.yml <<__END__
apiVersion: v1
kind: ImageStream
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: mysql
  name: mysql
spec:
  lookupPolicy:
    local: false
  tags:
  - annotations: null
    from:
      kind: DockerImage
      name: dockerreg.conygre.com:5000/trades/mysql:${p:DBVERSION}
    generation: null
    importPolicy:
      insecure: true
    name: ${p:DBVERSION}
status:
  dockerImageRepository: ""
__END__

    oc apply -f is.yml
    
    oc rollout latest dc/mysql || echo "Deployment already in progress"
fi